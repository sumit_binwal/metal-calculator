//
//  AppDelegate.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "AboutUsVC.h"
#import "LaserSelectVC.h"
#import "MailUsVC.h"
#import "CallUsVC.h"
#import "WebsiteVC.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize window,navController;
@synthesize footerView;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    dataDictMA=[[NSMutableDictionary alloc]init];
    LaserSelectVC *laserVC = [[LaserSelectVC alloc] init];
    self.navController = [[UINavigationController alloc] initWithRootViewController:laserVC];
    
    [self.navController setNavigationBarHidden:YES];
    self.window.rootViewController = navController;
    [window makeKeyAndVisible];
    [[DBManager getSharedInstance]checkAndCreateDatabase];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark--
#pragma mark-- TAbbar setup Methods
-(void)tabBarSetUp:(NSString *)tabAction{
    UINavigationController *navController1 = (UINavigationController *)self.window.rootViewController;
    [_btnWebsite setImage:[UIImage imageNamed:@"websitewhite"] forState:UIControlStateNormal];
    [_btnMail setImage:[UIImage imageNamed:@"mailuswhite"] forState:UIControlStateNormal];

    [_btnCall setImage:[UIImage imageNamed:@"calluswhite"] forState:UIControlStateNormal];

    [_btnAbout setImage:[UIImage imageNamed:@"aboutuswhite"] forState:UIControlStateNormal];
    [_btnAbout setBackgroundImage:nil forState:UIControlStateNormal];
    [_btnCall setBackgroundImage:nil forState:UIControlStateNormal];
    [_btnMail setBackgroundImage:nil forState:UIControlStateNormal];
    [_btnWebsite setBackgroundImage:nil forState:UIControlStateNormal];

    if ([tabAction isEqualToString:@"about"]) {
                  [_btnAbout setImage:[UIImage imageNamed:@"aboutusblack"] forState:UIControlStateNormal];
                  [_btnAbout setBackgroundImage:[UIImage imageNamed:@"hover_bg.png"] forState:UIControlStateNormal];

                  AboutUsVC *about=[[AboutUsVC alloc] init];
                  [navController1 pushViewController:about animated:YES];
        }else if ([tabAction isEqualToString:@"call"]){
            [_btnCall setImage:[UIImage imageNamed:@"callusblack"] forState:UIControlStateNormal];
            [_btnCall setBackgroundImage:[UIImage imageNamed:@"hover_bg.png"] forState:UIControlStateNormal];

            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"tel:+1519-776-9153"]];
            
        }else if ([tabAction isEqualToString:@"mail"]){
            [_btnMail setImage:[UIImage imageNamed:@"mailblack"] forState:UIControlStateNormal];
            [_btnMail setBackgroundImage:[UIImage imageNamed:@"hover_bg.png"] forState:UIControlStateNormal];

            // Email Subject
            NSString *emailTitle = @"";
            // Email Content
            NSString *messageBody = @"";
            // To address
            NSArray *toRecipents = [NSArray arrayWithObject:@"estimating@essexweld.com"];
            
            if (!self.mc) {
                self.mc = [[MFMailComposeViewController alloc] init];
                _mc.mailComposeDelegate = self;
            }
            
            
            [_mc setSubject:emailTitle];
            [_mc setMessageBody:messageBody isHTML:NO];
            [_mc setToRecipients:toRecipents];
            
            //self.mc = nil;
            
            NSArray *arrVC=self.navController.viewControllers;
            LaserSelectVC *topVC=(LaserSelectVC *)[arrVC objectAtIndex:arrVC.count-1];
            
            

            
            [topVC presentViewController:_mc animated:YES completion:nil];
        }else if ([tabAction isEqualToString:@"website"]){
            [_btnWebsite setImage:[UIImage imageNamed:@"websiteblack"] forState:UIControlStateNormal];
            [_btnWebsite setBackgroundImage:[UIImage imageNamed:@"hover_bg.png"] forState:UIControlStateNormal];

            
            
            WebsiteVC *websiteVC=[[WebsiteVC alloc] init];
            [self.navController pushViewController:websiteVC animated:YES];
        }
}


#pragma mark-- IBAction Methods

-(IBAction)footerAction:(UIButton *)btnSender{
    
    
        if (btnSender == _btnAbout) {
            [self tabBarSetUp:@"about"];
        }else if (btnSender == _btnCall){
            [self tabBarSetUp:@"call"];
        }else if (btnSender == _btnMail){
            [self tabBarSetUp:@"mail"];
        }else if (btnSender == _btnWebsite){
            [self tabBarSetUp:@"website"];
        }
}


-(void)showFooterView{
    NSLog(@"footer view frame is %@",NSStringFromCGRect(self.window.frame));
    [self.window bringSubviewToFront:footerView];
}
#pragma mark - Mail Deligate Method

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    NSArray *arrVC=self.navController.viewControllers;
    UIViewController *topVC=(UIViewController *)[arrVC objectAtIndex:arrVC.count-1];
    [topVC dismissViewControllerAnimated:YES completion:nil];

}

@end
