//
//  DBManager.m
//  FMDB Database_Image Insert
//
//  Created by Sumit Sharma on 12/10/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "DBManager.h"
#import "FMDatabase.h"
@interface DBManager ()

@end

@implementation DBManager

static DBManager *sharedInstance=nil;

+(DBManager *)getSharedInstance
{
    if(sharedInstance==nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedInstance=[[DBManager alloc]init];
        });
    }
    return sharedInstance;
}

#pragma mark -Database Methods
/**
 *  Check And Create Databse
 */
-(void)checkAndCreateDatabase
{
        NSString *documentDirectory=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
        databasePath=[documentDirectory stringByAppendingFormat:@"/%@",databaseName];
        if([[NSFileManager defaultManager]fileExistsAtPath:databasePath])
        {
            NSLog(@"databse Path : %@",databasePath);

            return;
        }
        NSString *databasePathFromApp=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:databaseName];
        NSError *error;
        [[NSFileManager defaultManager]copyItemAtPath:databasePathFromApp toPath:databasePath error:&error];
        if(error) {
            
            NSLog(@"The Error In Database Creation : ------- %@",error);
            
        }
        else
        {
            
            NSLog(@"Database SucessFully Copied");

        }
}
/**
 *  Fetch Category From Database
 *
 *  @return filterName, FilterID
 */
-(NSMutableArray *)fetchThicknessFromDB
{
    categoryMA=[[NSMutableArray alloc]init];
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"select * from flate"];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *thickness = [FmResultSet stringForColumn:@"thickness"];
            NSString *cost = [FmResultSet stringForColumn:@"cost"];
            
            [categoryMA addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:thickness,@"thickness",cost,@"cost", nil]];
        }
    }
    
    return categoryMA;
}

-(NSMutableArray *)fetchThicknessFromPipeDB
{
    categoryMA=[[NSMutableArray alloc]init];
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"select * from tube"];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *thickness = [FmResultSet stringForColumn:@"gauge"];
            NSString *cost = [FmResultSet stringForColumn:@"cost"];
            
            [categoryMA addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:thickness,@"thickness",cost,@"cost", nil]];
        }
    }
    
    return categoryMA;
}

-(NSMutableArray *)fetchGaugeFromDB:(int)calRange
{
    categoryMA=[[NSMutableArray alloc]init];
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"SELECT * FROM ranetable WHERE iscategory=%d",calRange];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *value = [FmResultSet stringForColumn:@"rangev"];
            [categoryMA addObject:value];
        }
    }
    return categoryMA;
}
-(NSMutableArray *)fetchThicknessFromFlateDB
{
    categoryMA=[[NSMutableArray alloc]init];
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"select * from flattb"];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *thickness = [FmResultSet stringForColumn:@"thickness"];
            NSString *cost = [FmResultSet stringForColumn:@"cost"];
            
            [categoryMA addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:thickness,@"thickness",cost,@"cost", nil]];
        }
    }
    
    return categoryMA;
}

@end
