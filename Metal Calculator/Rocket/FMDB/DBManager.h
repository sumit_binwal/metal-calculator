//
//  DBManager.h
//  FMDB Database_Image Insert
//
//  Created by Sumit Sharma on 12/10/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface DBManager : NSObject
{
        NSMutableArray *categoryMA;
    
}
+(DBManager *)getSharedInstance;
-(void)checkAndCreateDatabase;
-(NSMutableArray *)fetchThicknessFromDB;
-(NSMutableArray *)fetchThicknessFromPipeDB;
-(NSMutableArray *)fetchGaugeFromDB:(int)calRange;
-(NSMutableArray *)fetchThicknessFromFlateDB;
@end
