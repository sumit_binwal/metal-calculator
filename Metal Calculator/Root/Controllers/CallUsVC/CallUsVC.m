//
//  CallUsVC.m
//  animation3d
//
//  Created by Tanvi on 26/11/14.
//  Copyright (c) 2014 Konstant Info Solutions. All rights reserved.
//

#import "CallUsVC.h"

@interface CallUsVC ()

@end

@implementation CallUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"tel:+918787878787"]];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
