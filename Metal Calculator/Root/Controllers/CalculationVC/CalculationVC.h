//
//  CalculationVC.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 27/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculationVC : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,UIAlertViewDelegate>
{
    NSString *strDensity;
    NSString *strMaterial;
}
@property (strong, nonatomic) IBOutlet UIView *VwCntrller;
- (IBAction)calculateButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblTypeDensity;
@property (strong, nonatomic) IBOutlet UILabel *lblDensity;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (strong, nonatomic) IBOutlet UILabel *lblUnit2;
@property (strong, nonatomic) IBOutlet UILabel *lblUnit3;
@property (strong, nonatomic) IBOutlet NSString *strDensity;
@property (strong, nonatomic) IBOutlet NSString *strMaterial;
@property (strong, nonatomic) IBOutlet UIImageView *imgVw;
@property (strong, nonatomic) IBOutlet UILabel *lblThickness;
@property (strong, nonatomic) IBOutlet UILabel *lblMM;

@property (strong, nonatomic) IBOutlet UILabel *lblUnit1;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentController;
@property (strong, nonatomic) IBOutlet UITextField *txtLength;
@property (strong, nonatomic) IBOutlet UITextField *txtWidth;
@property (strong, nonatomic) IBOutlet UITextField *RequiredInch;
@property (strong, nonatomic) IBOutlet UILabel *lblMaterial;



- (IBAction)segController:(UISegmentedControl *)sender forEvent:(UIEvent *)event;


@end
