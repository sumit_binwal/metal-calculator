//
//  CalcResultVC.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 27/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "CalcResultVC.h"
#import "RequestVC.h"
@interface CalcResultVC ()
{
    float calculateResult;
}
@end

@implementation CalcResultVC
@synthesize imgVw;
@synthesize calculationResult,requiredInch;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    [imgVw setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"imgName"]]]];
    [_lblResult setText:[NSString stringWithFormat:@"$ %@ Cost/Inch",calculationResult]];
    calculateResult=[calculationResult floatValue];
    calculateResult=calculateResult*requiredInch;
    
    [_lblResult1 setText:[NSString stringWithFormat:@"$ %.02f Cost/%.02f Inch",calculateResult,requiredInch]];
    // Do any additional setup after loading the view from its nib.
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Result"];
    UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)requestButtonClicked:(id)sender {
    RequestVC *request=[[RequestVC alloc]init];
  //  request.resultValue=calculateResult;
    [self.navigationController pushViewController:request animated:YES];
}
@end
