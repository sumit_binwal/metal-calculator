//
//  CalcResultVC.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 27/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalcResultVC : UIViewController
{
    NSString *calculationResult;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgVw;
@property(strong,nonatomic)NSString* calculationResult;
@property (strong, nonatomic) IBOutlet UILabel *lblResult1;
- (IBAction)requestButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblResult;
@property(assign,nonatomic)float requiredInch;
@end
