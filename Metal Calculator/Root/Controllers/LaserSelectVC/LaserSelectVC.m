//
//  LaserSelectVC.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "LaserSelectVC.h"
#import "SelectShapeVC.h"
#import "AppDelegate.h"

@interface LaserSelectVC ()
@property (strong, nonatomic) IBOutlet UIButton *flatLaser;
- (IBAction)flatLaserButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *tubeLaser;
- (IBAction)tubeLaserButtonClicked:(id)sender;


@end

@implementation LaserSelectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
    _flatLaser.layer.cornerRadius=20;
    _tubeLaser.layer.cornerRadius=20;
    AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
    [appdelegate showFooterView];

    
}


-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Select Laser" ];
    UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
//    [self.flatLaser setImage:[UIImage imageNamed:@"tube_laser"] forState:UIControlStateHighlighted];
//    [self.tubeLaser setImage:[UIImage imageNamed:@"tube_laser"] forState:UIControlStateHighlighted];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)flatLaserButtonClicked:(id)sender {
    SelectShapeVC *shapeVC=[[SelectShapeVC alloc]init];
    [[NSUserDefaults standardUserDefaults]setValue:@"flatbed Laser" forKey:@"shape"];
    [[NSUserDefaults standardUserDefaults]synchronize ];
    NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"flatbed Laser",@"shape", nil];
    [dataDictMA setValuesForKeysWithDictionary:tempDict];
    [self.navigationController pushViewController:shapeVC animated:YES];
}
- (IBAction)tubeLaserButtonClicked:(id)sender {
    SelectShapeVC *shapeVC=[[SelectShapeVC alloc]init];
    
    [[NSUserDefaults standardUserDefaults]setValue:@"Tube Laser" forKey:@"shape"];
    [[NSUserDefaults standardUserDefaults]synchronize ];
    NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"Tube Laser",@"shape", nil];
    [dataDictMA setValuesForKeysWithDictionary:tempDict];
    [self.navigationController pushViewController:shapeVC animated:YES];

}
@end
