//
//  SelectRangeVC.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 05/12/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectRangeVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
