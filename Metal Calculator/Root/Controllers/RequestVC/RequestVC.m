//
//  RequestVC.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 27/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "RequestVC.h"

@interface RequestVC ()
{
    BOOL scrollingonKeyboardAppear;
    UIBarButtonItem *doneBtn;
    UIBarButtonItem *cancelBtn;
    UITextField *activeTextField;
}
@end

@implementation RequestVC
@synthesize resultValue;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
    NSLog(@"%@",resultValue);
    _txtAmount.text =[NSString stringWithFormat:@"%@",resultValue];
    _txtAmount.enabled=NO;

    
}
-(void)viewDidLayoutSubviews
{
    if(scrollingonKeyboardAppear)
    {
        [self.scrollVw setContentSize:CGSizeMake(320.0f, 750.0f)];
        
    }
    else
    {
        [self.scrollVw setContentSize:CGSizeMake(320.0f, 600.0f)];
    }
    
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Request Quote"];
    UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    [numberToolbar setBarTintColor:[UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1.0f]];
    [numberToolbar setTranslucent:NO];
    
    cancelBtn = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancleButton)];
    doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(doneButton)];
    
    
    numberToolbar.items = [NSArray arrayWithObjects:cancelBtn, [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneBtn, nil];
    
    [numberToolbar sizeToFit];
    
    _txtPhone.inputAccessoryView = numberToolbar;
    
    
    if([[UIScreen mainScreen]bounds].size.height<568)
    {
        [self.scrollVw setScrollEnabled:YES];
    }
    else
    {
        [self.scrollVw setScrollEnabled:NO];
    }
}
-(void)cancleButton
{
    [_txtPhone resignFirstResponder];
    [self scrollToNormalView];
}
-(void)doneButton
{
    [_txtState becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UIScroll View Deligate
- (void) scrollViewToCenterOfScreen:(UITextField *)txtField
{
    float difference;
    
    if (self.scrollVw.contentSize.height == 600.0f)
        difference = 350.0f;
    else
        difference = 300.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 190.0f;
    CGFloat y = viewCenterY - avaliableHeight / 2.0f;
    
    if (y < 0)
        y = 0;
    
    [self.scrollVw setContentOffset:CGPointMake(0, y) animated:TRUE];
    scrollingonKeyboardAppear=YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_txtSpecialInstruction)
    {
        [_txtName becomeFirstResponder];
        return YES;
    }
    if(textField==_txtName)
    {
        [_txtCompany becomeFirstResponder];
        return YES;
    }
    if(textField==_txtCompany)
    {
        [_txtEmail becomeFirstResponder];
        return YES;
    }
    if(textField==_txtEmail)
    {
        [_txtPhone becomeFirstResponder];
        return YES;
    }
    if(textField==_txtPhone)
    {
        [_txtState becomeFirstResponder];
        return YES;
    }
    if(textField==_txtState)
    {
        [_txtState resignFirstResponder];
        [self scrollToNormalView];
    scrollingonKeyboardAppear=NO;
        
        return YES;
            }
    
    return YES;
}

-(BOOL)isValidatedTextFieldValue
{
if(![CommonFunctions isValueNotEmpty:_txtSpecialInstruction.text])
{
    [CommonFunctions alertTitle:@"" withMessage:@"Please enter special instruction " withDelegate:self];
    activeTextField=_txtSpecialInstruction;
    return NO;
}
    if(![CommonFunctions isValueNotEmpty:_txtName.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter your name" withDelegate:self];
    activeTextField=_txtName;
        return NO;
    }
    if(![CommonFunctions isValueNotEmpty:_txtCompany.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter your company" withDelegate:self];
    activeTextField=_txtCompany;
        return NO;
    }
    if(![CommonFunctions isValueNotEmpty:_txtEmail.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter your email" withDelegate:self];
    activeTextField=_txtEmail;
        return NO;
    }
    if(![CommonFunctions IsValidEmail:_txtEmail.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter a valid email address" withDelegate:self];
    activeTextField=_txtEmail;
        return NO;
    }
    if(![CommonFunctions isValueNotEmpty:_txtPhone.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter your phone number" withDelegate:self];
    activeTextField=_txtPhone;
        return NO;
    }
    if(![CommonFunctions isValueNotEmpty:_txtState.text])
        
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter state" withDelegate:self];
    activeTextField=_txtState;
        return NO;
    }
    return YES;
}
-(void)scrollToNormalView
{
    [self.scrollVw setContentSize:CGSizeMake(320.0f, 500.0f)];
    if([[UIScreen mainScreen]bounds].size.height<568)
    {
        [self.scrollVw setScrollEnabled:YES];
    }
   else
    {
        [self.scrollVw setScrollEnabled:NO];
    }
}

#pragma mark - UI TextField Deligate Method
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self scrollViewToCenterOfScreen:textField];
    [self.scrollVw setScrollEnabled:YES];
if(textField==_txtPhone || textField==_txtState )
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
    [self.scrollVw setContentSize:CGSizeMake(320.0f, 750.0f)];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"Text Field Did End Editing Call");

}
-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"view frame %@",NSStringFromCGRect(self.view.frame));
    NSLog(@"scroll view frame is %@",NSStringFromCGRect(_scrollVw.frame));
    [super viewDidAppear:animated]; 
    _containerView.frame = CGRectMake(0, 0, _scrollVw.frame.size.width, _scrollVw.frame.size.height);
    NSLog(@"container frame is %@",NSStringFromCGRect(_containerView.frame));
    [_scrollVw addSubview:_containerView];
}
- (IBAction)sendMsgButtonClicked:(id)sender {
    [self.view endEditing:YES];
    [_scrollVw setContentOffset:CGPointZero];
    if([self isValidatedTextFieldValue])
    {
        // Email Subject
        NSString *emailTitle = @"Metal Weight Calculator - Request Quota";
        // Email Content
        NSString *messageBody = [NSString stringWithFormat:@"<b> Special Instruction : </b> %@<br><b> Name : </b> %@<br><b> Company : </b> %@<br><b> Email  : </b> %@<br><b> Phone : </b> %@<br><b> State : </b> %@<br><b>Metal Weight Details : </b><hr><b>Laser :</b> %@ <br><hr><b>Shape Name </b>: %@<br><hr><b>Material :</b> %@<br><hr><b>Thickness :</b> %@<br><hr><b>Required Inches :</b> %@<br><hr><b> Amount : </b> %@<br>",_txtSpecialInstruction.text,_txtName.text,_txtCompany.text,_txtEmail.text,_txtPhone.text,_txtState.text,[dataDictMA objectForKey:@"shape"],[dataDictMA objectForKey:@"shapeName"],[dataDictMA objectForKey:@"Material"],[dataDictMA objectForKey:@"thickness"],[dataDictMA objectForKey:@"rInch"],_txtAmount.text];
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"estimating@essexweld.com"];
    
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:YES];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    }
#pragma mark - Mail Deligate Method

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark - UIAlert View Delegate Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(activeTextField==_txtAmount)
    {
        [_txtAmount becomeFirstResponder];
    }
    else if (activeTextField==_txtCompany)
    {
        [_txtCompany becomeFirstResponder];
    }
    else if (activeTextField==_txtEmail)
    {
        [_txtEmail becomeFirstResponder];
    }
    else if (activeTextField==_txtName)
    {
        [_txtName becomeFirstResponder];
    }
    else if (activeTextField==_txtPhone)
    {
        [_txtPhone becomeFirstResponder];
    }
    else if (activeTextField==_txtSpecialInstruction)
    {
        [_txtSpecialInstruction becomeFirstResponder];
    }
    else if (activeTextField==_txtState)
    {
        [_txtState becomeFirstResponder];
    }
}
@end
