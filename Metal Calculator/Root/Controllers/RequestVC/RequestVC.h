//
//  RequestVC.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 27/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestVC : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,MFMailComposeViewControllerDelegate,UIAlertViewDelegate>
{
//    NSString *resultValue;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (strong, nonatomic) NSString *resultValue;
@property (strong, nonatomic) IBOutlet UITextField *txtAmount;
@property (strong, nonatomic) IBOutlet UITextField *txtSpecialInstruction;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtCompany;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;
@property (strong, nonatomic) IBOutlet UITextField *txtState;

- (IBAction)sendMsgButtonClicked:(id)sender;

@property (nonatomic, strong)     IBOutlet    UIView      *containerView;

@end
