//
//  RectTubeVC.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 23/02/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RectTubeVC : UIViewController
- (IBAction)squareButtonClicked:(id)sender;
- (IBAction)rectangleButtonClicked:(id)sender;



@end
