//
//  RectTubeVC.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 23/02/15.
//  Copyright (c) 2015 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "RectTubeVC.h"
#import "SelectMaterialVC.h"
@interface RectTubeVC ()

@end

@implementation RectTubeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Select Tube"];
    UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)squareButtonClicked:(id)sender {
    SelectMaterialVC *smvc=[[SelectMaterialVC alloc]init];
    shape=@"square";
    [self.navigationController pushViewController:smvc animated:YES];
    
}

- (IBAction)rectangleButtonClicked:(id)sender {
    SelectMaterialVC *smvc=[[SelectMaterialVC alloc]init];
        shape=@"rectangle";
    [self.navigationController pushViewController:smvc animated:YES];
}
@end
