//
//  SelectShapeVC.m
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "SelectShapeVC.h"
#import "SelectMaterialVC.h"
#import "RectTubeVC.h"
@interface SelectShapeVC ()

- (IBAction)flatBarButtonClicked:(id)sender;
- (IBAction)sheetButtonClicked:(id)sender;


@end

@implementation SelectShapeVC
@synthesize shape;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"shape"]isEqualToString:@"Tube Laser"])
       {
           [_txtLbl1 setText:@"Pipe (O.D)"];
           [_txtLbl2 setText:@"Square/Rectangle Tube "];
           [_imgVw2 setImage:[UIImage imageNamed:@"squaretube"]];
           [_imgVw1 setImage:[UIImage imageNamed:@"pipe"]];
       }
        else
        {
            [_txtLbl1 setText:@"Sheet/Plate"];
            [_imgVw2 setImage:[UIImage imageNamed:@"sheetPlat"]];
            [_txtLbl2 setHidden:YES];
            [_btnVw2 setHidden:YES];
            [_imgVw2 setHidden:YES];
        }
    

  
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"Select Shape"];
    UIImage* image = [UIImage imageNamed:@"logo_icon"];
    CGRect frameimg = CGRectMake(0,0, 71,40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem =mailbutton;
    
   self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    

}
-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)flatBarButtonClicked:(id)sender {
    SelectMaterialVC *materialVC=[[SelectMaterialVC alloc]init];
if([[[NSUserDefaults standardUserDefaults]valueForKey:@"shape"]isEqualToString:@"Tube Laser"])
{
    [[NSUserDefaults standardUserDefaults]setValue:@"pipe" forKey:@"imgName"];
    
    NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"pipe",@"imgName",_txtLbl1.text,@"shapeName", nil];
    
    [dataDictMA setValuesForKeysWithDictionary:tempDict];
}
    else
    {
           [[NSUserDefaults standardUserDefaults]setValue:@"sheetPlat" forKey:@"imgName"];
    NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"pipe",@"imgName",_txtLbl1.text,@"shapeName", nil];
    [dataDictMA setValuesForKeysWithDictionary:tempDict];
        
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController pushViewController:materialVC animated:YES];
}

- (IBAction)sheetButtonClicked:(id)sender {
//    SelectMaterialVC *materialVC=[[SelectMaterialVC alloc]init];
    RectTubeVC *rtVC=[[RectTubeVC alloc]init];
    
    [[NSUserDefaults standardUserDefaults]setValue:@"squaretube" forKey:@"imgName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"pipe",@"imgName",_txtLbl2.text,@"shapeName", nil];

    [dataDictMA setValuesForKeysWithDictionary:tempDict];

    [self.navigationController pushViewController:rtVC animated:YES];
}
@end
