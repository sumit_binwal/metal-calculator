//
//  SelectShapeVC.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectShapeVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *txtLbl1;
@property (strong, nonatomic) IBOutlet UIButton *btnVw2;

@property (strong, nonatomic) IBOutlet UILabel *txtLbl2;


@property (strong, nonatomic) IBOutlet UIImageView *imgVw1;
@property (strong, nonatomic) IBOutlet UIImageView *imgVw2;
@property(nonatomic,strong)NSString *shape;
@end
