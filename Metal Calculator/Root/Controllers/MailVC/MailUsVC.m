//
//  MailUsVC.m
//  animation3d
//
//  Created by Tanvi on 26/11/14.
//  Copyright (c) 2014 Konstant Info Solutions. All rights reserved.
//

#import "MailUsVC.h"
#import <MessageUI/MessageUI.h>
@interface MailUsVC ()<MFMailComposeViewControllerDelegate>

@end

@implementation MailUsVC
#pragma mark -
#pragma mark -  life cycle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated{
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    
    if ([MFMailComposeViewController canSendMail]) {
        [mc setSubject:@""];
        [mc setMessageBody:@"" isHTML:NO];
        // Determine the MIME type
        [mc setToRecipients:@[@"estimating@essexweld.com"]];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:^{
      //      [activity hide:YES];
            
            //[((AppDelegate*)NewsAppDelegate) hideProgressIndicatorOn:self];
        }];
    }
    else{
       // [activity hide:YES];
        
        //            [((AppDelegate*)NewsAppDelegate) hideProgressIndicatorOn:self];
    }
}
#pragma mark -
#pragma mark -  mail composervc delegate methods
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            
            
            
            
            break;
        case MFMailComposeResultSaved:
            
          //  [CommonFunctions alertTitle:@"" withMessage:@"Mail saved."];
            
            
            
            break;
        case MFMailComposeResultSent:
            
         //   [CommonFunctions alertTitle:@"" withMessage:@"E-Mail Sucessfully Sent"];
            
            
            break;
        case MFMailComposeResultFailed:
            
           // [CommonFunctions alertTitle:@"mail" withMessage:error.localizedDescription];
            
            
            
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
