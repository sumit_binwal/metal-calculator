//
//  Config.h
//  Metal Calculator
//
//  Created by Sumit Sharma on 26/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject
extern NSString *databaseName;
extern NSString *databasePath;
extern NSString *materialCost;
extern NSString *shape;
extern NSMutableDictionary *dataDictMA;
@end
